#include <iostream>
#include <iomanip>
#include <fstream>
#include "itensor/all.h"
#include "kondo_heisenberg.h"
#include "test.h"
#include "tdvp.h"

using namespace itensor;

void test()
{
    KHTest::t00 = 0.0;
    KHTest::U = 0.0;
    KHTest::K = 1.0;
    KHTest::N = 4;
    KHTest::generateMatrices();
    KHTest::generateHamiltonian();
    KHTest::calculateGS();
    output << "Test E0 = " << KHTest::E0 << std::endl;
}

void TDVP()
{
    int N = 4;
    auto t = 0.1;

    double t00 = 1.0;
    double U   = 1.0;
    double K   = 1.0;
    double JH  = 1.0;

    seedRNG(1);
    auto sites = KondoHeisenberg(N);
    auto state = InitState(sites);

    for(int i=1; i<=N; i+=2){
       state.set(i,"U_D");
       state.set(i+1,"D_D");
    }
    auto psi1 = MPS(state);

    printfln("-------------------------------------DMRG warm-up----------------------------");

    auto sweeps = Sweeps(4);
    sweeps.maxdim() = 3000;
    sweeps.cutoff() = 1E-12;
    sweeps.niter() = 20;

    auto ampo = AutoMPO(sites);
    for(int j=1; j<N; ++j){
        ampo += t00,"cT_0,u",j,"c_0,u",j+1;
        ampo += t00,"c_0,u",j,"cT_0,u",j+1;
        ampo += t00,"cT_0,d",j,"c_0,d",j+1;
        ampo += t00,"c_0,d",j,"cT_0,d",j+1;

        ampo += K/2,"s+_1",j,"s-_1",j+1;
        ampo += K/2,"s-_1",j,"s+_1",j+1;
        ampo += K,"sz_1",j,"sz_1",j+1;
    }

    for(int j=1; j<=N; ++j){
        ampo += U,"n_0,ud",j;
        ampo += -2.0*JH,"s_01",j;
    }

    auto H = toMPO(ampo);

    out << "Energy of psi1: " << real(innerC(psi1,H,psi1)) << std::endl;

    dmrg(psi1,H,sweeps,{"Silent",true});

    out << "Energy after DMRG: " << real(innerC(psi1,H,psi1)) << std::endl;

    auto energy = tdvp(psi1,H,-t,sweeps,{"DoNormalize",true,
                                        "Quiet",true,
                                        "NumCenter",2});

    out << "Energy TVDP: " << energy << std::endl;
}

int main ()
{
   // test();
    TDVP();
    return 0;
}
