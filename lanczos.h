#ifndef LANCZOS
#define LANCZOS

#include <iostream>
#include <iomanip>
#include <cassert>
#include <complex>

typedef std::complex<double> complex;

#define output std::cout << std::fixed << std::setprecision(7)

typedef arma::mat matrix;
typedef arma::vec vector;
typedef arma::rowvec vectorT;
typedef double var;

struct Eigen
{
    Eigen(double value, vector vector) :
        Value{value}
      , Vector{vector}
      , VectorT{vector.t()}
    {

    }
    double Value;
    vector Vector;
    vectorT VectorT;
};

std::vector<Eigen> exactDiagonalization(const matrix &mat)
{
    std::vector<Eigen> oEigens;
    matrix M;
    arma::vec V;
    eig_sym(V,M,mat);
    for(uint i=0; i<V.size(); i++){
        oEigens.push_back( {V[i], M.col(i)});
    }
    return oEigens;
}

uint mrank(const matrix &matrix)
{
    return (uint)sqrt(matrix.size());
}

vector randomVector(uint nSize)
{
    vector startVec = arma::randn<vector>(nSize);
    return normalise(startVec);
}

matrix prepareLanczosMatrix(vector &a, vector &b)
{
    uint M = a.size();
    matrix lMatrix{ M, M, arma::fill::zeros };
    for(uint i=0; i<M-1; i++){
        lMatrix(i,i) = a[i];
        lMatrix(i,i+1) = b[i+1];
        lMatrix(i+1,i) = b[i+1];
    }
    lMatrix(M-1,M-1) = a[M-1];

    return lMatrix;
}

void prepareLanczosVectors(vector &a,
                           vector &b,
                           const matrix &Mat,
                           vector startVector,
                           uint M,
                           matrix &V)
{
    a.resize(M); a.zeros(); b.resize(M); b.zeros();
    V.resize(mrank(Mat), M);
    V.col(0) = normalise(startVector);

    vector tmp = Mat*V.col(0);



    a[0] = as_scalar(V.col(0).t()*tmp);
    b[0] = 0.0;
    V.col(1) = tmp - a[0]*V.col(0);
    b[1] = norm(V.col(1));
    V.col(1) = V.col(1)/b[1];


    for(uint i=1; i<M-1; i++){
        tmp = Mat*V.col(i);

        a[i] = as_scalar(V.col(i).t()*tmp);
        b[i] = as_scalar(V.col(i-1).t()*tmp);
        V.col(i+1) = tmp - a[i]*V.col(i)-b[i]*V.col(i-1);
        b[i+1] = norm(V.col(i+1));
        V.col(i+1) = V.col(i+1)/b[i+1];
    }
    a[M-1] = as_scalar(V.col(M-1).t()*Mat*V.col(M-1));
}


std::vector<Eigen> lanczos(const matrix &mat, vector startVector, uint M, matrix &V)
{
    if(M>mrank(mat)){ M = mrank(mat); }
    vector a, b;

    prepareLanczosVectors(a,b,mat,startVector,M, V);
    matrix T = prepareLanczosMatrix(a,b);

    return exactDiagonalization(T);
}

std::vector<Eigen> lanczos(const matrix &mat, uint M, matrix &V)
{
    return lanczos(mat, randomVector(mrank(mat)), M, V);
}

#endif // LANCZOS
