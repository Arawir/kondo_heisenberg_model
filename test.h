#ifndef __TEST_H
#define __TEST_H

#include <armadillo>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cassert>

#include "lanczos.h"

#define out std::cout << std::fixed << std::setprecision(7)


namespace KHTest{
    double t00=1.0;
    double U=1.0;
    double K=1.0;
    int N=6;

    double E0;

    arma::mat C_0u{4,4,arma::fill::zeros};
    arma::mat C_0d{4,4,arma::fill::zeros};
    arma::mat N_0ud{4,4,arma::fill::zeros};
    arma::mat Sp_1{2,2,arma::fill::zeros};
    arma::mat Sm_1{2,2,arma::fill::zeros};
    arma::mat Sz_1{2,2,arma::fill::zeros};

    arma::mat S_1{4,4,arma::fill::zeros};
    arma::mat C_0{16,16,arma::fill::zeros};

    arma::mat H0;
    arma::mat H1;

    arma::mat I2(uint nodes)
    {
        return arma::mat{(uint)pow(2,nodes),(uint)pow(2,nodes),arma::fill::eye};
    }
    arma::mat I4(uint nodes)
    {
        return arma::mat{(uint)pow(4,nodes),(uint)pow(4,nodes),arma::fill::eye};
    }

    void generateMatrices()
    {
        C_0u(0,1) = 1.0;
        C_0u(2,3) = 1.0;

        C_0d(0,2) = 1.0;
        C_0d(1,3) = 1.0;

        N_0ud(3,3) = 1.0;

        Sp_1(0,1) = 1.0;
        Sm_1(1,0) = 1.0;
        Sz_1(0,0) = 0.5;
        Sz_1(1,1) = -0.5;

        S_1 = 0.5*kron(Sp_1,Sm_1) + 0.5*kron(Sm_1,Sp_1) + kron(Sz_1,Sz_1);

        C_0 = kron(C_0u.t(),C_0u) + kron(C_0u,C_0u.t()) + kron(C_0d.t(),C_0d) + kron(C_0d,C_0d.t());
    }

    void generateHamiltonian()
    {
        H1 = K*kron( S_1, I2(N-2));
        for(uint i=1; i<N-1; i++){
            H1 += K*kron(kron( I2(i), S_1 ), I2(N-i-2));
        }

        H0 = t00*kron( C_0, I4(N-2) );
        for(uint i=1; i<N-1; i++){
            H0 += t00*kron(kron( I4(i), C_0 ), I4(N-i-2));
        }

        H0 += U*kron(N_0ud, I4(N-1) );
        for(uint i=1; i<N; i++){
            H0 += U*kron(kron( I4(i), N_0ud ), I4(N-i-1));
        }
    }

    void calculateGS()
    {
        arma::mat V;
        E0 = 0.0;

        if((K!=0.0)||(U!=0.0)){
            E0 = lanczos(H0,40,V).at(0).Value;
        }
        if(abs(K)>0.001){
            E0 += lanczos(H1,40,V).at(0).Value;
        }
    }
}


#endif
